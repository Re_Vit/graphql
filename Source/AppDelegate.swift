//
//  AppDelegate.swift
//  GraphQLSampleProject
//
//  Created by reztsov on 9/13/18.
//  Copyright © 2018 reztsov. All rights reserved.
//

import UIKit
import Apollo

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let apollo = ApolloClient(url: URL(string: "https://api.graph.cool/simple/v1/cjmg2ljqd4ofg01906b4ruvpz")!)


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let navigation = NavigationController()
        let viewC = LoginViewController.loadFromStoryboard()
        navigation.pushViewController(viewC, animated: false)
        window?.rootViewController = navigation

        return true
    }
}

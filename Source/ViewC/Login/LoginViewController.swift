//
//  LoginViewController.swift
//  GraphQLSampleProject
//
//  Created by reztsov on 9/25/18.
//  Copyright © 2018 reztsov. All rights reserved.
//

import Foundation
import UIKit
import Apollo

final class LoginViewController: UIViewController, StoryboardLoadable {

    // MARK: - @IBOutlet
    @IBOutlet private weak var emailLbl: UILabel!
    @IBOutlet private weak var emailTextfield: UITextField!
    @IBOutlet private weak var passwordLbl: UILabel!
    @IBOutlet private weak var passwordTextField: UITextField!

    // MARK: - Properties

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - UI
extension LoginViewController {

    func setupTextfields() {
        emailTextfield.delegate = self
        passwordTextField.delegate = self
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === emailTextfield {
            emailTextfield.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
            return true
        } else {
            return true
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField === emailTextfield {

        } else {
            
        }
    }
}
